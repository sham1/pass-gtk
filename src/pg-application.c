/* pg-application.c
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pg-application.h"
#include "pg-password-file.h"
#include "pg-store.h"
#include "pg-filtered-listmodel.h"

struct _PgApplication
{
	GtkApplication parent_instance;

	PgStore *store;

	GtkStack *content_stack;
	GtkStack *title_stack;
	GListStore *password_dirs;
	PgFilteredListmodel *password_dirs_filtered;

	GtkSearchBar *password_search_bar;
	GtkSearchEntry *password_search_entry;

	gchar *current_search_term;
};

G_DEFINE_TYPE (PgApplication, pg_application, GTK_TYPE_APPLICATION)

static void
pg_application_get_password_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
	PgStore *store = (PgStore *) object;

	g_assert (PG_IS_STORE (store));
	g_assert (G_IS_ASYNC_RESULT (result));

	g_autoptr (GError) error = NULL;
	gchar *pass = pg_store_get_password_finish (store, result, &error);
	if (pass == NULL) {
		fprintf (stderr, "Couldn't get password: %s\n",
			 error->message);
		return;
	}
	GtkClipboard *clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_text (clipboard, pass, strlen (pass));
}

static void
pg_application_get_passwords_cb2 (gpointer data, gpointer user_data)
{
	GListStore *password_dirs = user_data;
	PgPasswordFile *password = data;
	g_list_store_append (password_dirs, password);
}

static void
pg_application_get_passwords_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
	PgStore *store = (PgStore *) object;
	g_assert (PG_IS_STORE (store));

	PgApplication *self = user_data;

	g_autoptr (GError) error = NULL;
	GPtrArray *passwords = pg_store_get_passwords_finish (store,
							      result,
							      &error);
	if (passwords == NULL) {
		g_debug ("Couldn't get passwords from the database: %s\n",
			 error->message);
		gtk_stack_set_visible_child_name (self->content_stack,
						  "welcome-screen");
		gtk_stack_set_visible_child_name (self->title_stack,
						  "welcome-screen");
		return;
	}
	gtk_stack_set_visible_child_name (self->content_stack,
					  "password-list");
	gtk_stack_set_visible_child_name (self->title_stack,
					  "password-list-title");
	g_ptr_array_foreach (passwords,
			     pg_application_get_passwords_cb2,
			     self->password_dirs);
	g_ptr_array_unref (passwords);
}

static gchar *
pg_application_get_path_without_extension (gchar *path)
{
	g_assert (path != NULL);
	size_t len = strlen (path);
	ssize_t pos = -1;
	for (size_t i = len - 1; i >= 0; --i) {
		if (path[i] == '.') {
			pos = i;
			break;
		}
	}
	if (pos == -1) return g_strdup (path);
	gchar *tmp = g_strdup (path);
	tmp[pos] = '\0';
	gchar *ret = g_strdup (tmp);
	g_free (tmp);
	return ret;
}

static GtkWidget *
show_password_cb (gpointer item,
                  gpointer user_data)
{
	PgPasswordFile *password_file = item;
	gchar *password_path =
		pg_password_file_get_backing_file (password_file);
	gchar *path_without_ext =
		pg_application_get_path_without_extension (password_path);
	GtkLabel *label = (GtkLabel *) gtk_label_new (path_without_ext);
	g_free (path_without_ext);
	g_object_set_data_full (G_OBJECT (label),
				"pass-path",
				g_object_ref (password_file),
				g_object_unref);
	return GTK_WIDGET (label);
}

static void
pg_application_activate_password_file (GtkListBox    *box,
                                       GtkListBoxRow *row,
                                       gpointer       user_data)
{
	PgApplication *self = user_data;
	PgStore *store = self->store;
	GtkLabel *label = (GtkLabel *) gtk_bin_get_child (GTK_BIN (row));

	PgPasswordFile *password_file = g_object_get_data (G_OBJECT (label),
							   "pass-path");

	g_assert (PG_IS_APPLICATION (self));
	g_assert (PG_IS_STORE (store));
	g_assert (GTK_IS_LABEL (label));
	g_assert (PG_IS_PASSWORD_FILE (password_file));

	pg_store_get_password_async (store,
				     password_file,
				     NULL,
				     pg_application_get_password_cb,
				     NULL);
}

static gboolean
search_filter_predicate_cb (gpointer item,
                            gpointer user_data)
{
	PgApplication *app = user_data;

	if (app->current_search_term == NULL ||
	    strlen (app->current_search_term) == 0) return TRUE;

	PgPasswordFile *file = item;
	gchar *backing = pg_password_file_get_backing_file (file);

	return strstr (backing, app->current_search_term) != NULL;
}

static void
search_changed_cb (GtkSearchEntry *entry,
                   gpointer        user_data)
{
	PgApplication *app = user_data;

	g_free (app->current_search_term);
	app->current_search_term = g_strdup (gtk_entry_get_text (
					     GTK_ENTRY (entry)));

	pg_filtered_listmodel_flush_filter (app->password_dirs_filtered);
}

static void
stop_search_cb (GtkSearchEntry *entry,
                gpointer        user_data)
{
	PgApplication *app = user_data;
	g_clear_pointer (&app->current_search_term, g_free);
	pg_filtered_listmodel_flush_filter (app->password_dirs_filtered);
}

static gboolean
window_key_press_event_cb (GtkWidget *widget,
                           GdkEvent  *event,
                           gpointer   user_data)
{
	PgApplication *app = user_data;
	return gtk_search_bar_handle_event (app->password_search_bar, event);
}

static void
search_button_toggled_cb (GtkToggleButton *button,
                          gpointer         user_data)
{
	GtkSearchBar *bar = user_data;

	gtk_search_bar_set_search_mode (bar,
					gtk_toggle_button_get_active (button));
}

static void
search_bar_mode_changed_cb (GObject    *obj,
                            GParamSpec *pspec,
                            gpointer    user_data)
{
	GtkSearchBar *bar = (GtkSearchBar *) obj;
	GtkToggleButton *button = user_data;

	gtk_toggle_button_set_active (button,
				      gtk_search_bar_get_search_mode (bar));
}

static void
pg_application_activate (GApplication *app)
{
	PgApplication *self = (PgApplication *) app;
	g_assert (PG_IS_APPLICATION (self));

	GtkBuilder *builder =
		gtk_builder_new_from_resource
		    ("/fi/sinervo/sham1/passgtk/pass-gtk.glade");

	GtkApplicationWindow *win = (GtkApplicationWindow *)
		gtk_builder_get_object (builder, "pg-mainwindow");
	GtkListBox *passwords_box = (GtkListBox *)
		gtk_builder_get_object (builder, "pg-passwordlistbox");

	self->content_stack = (GtkStack *)
		gtk_builder_get_object (builder, "pg-modestack");
	self->title_stack = (GtkStack *)
		gtk_builder_get_object (builder, "pg-titlestack");

	self->password_search_bar = (GtkSearchBar *)
		gtk_builder_get_object (builder, "pg-passwordsearchbar");
	self->password_search_entry = (GtkSearchEntry *)
		gtk_builder_get_object (builder, "pg-passwordsearchentry");

	self->password_dirs = g_list_store_new (PG_TYPE_PASSWORD_FILE);
	self->password_dirs_filtered =
		pg_filtered_listmodel_new (G_LIST_MODEL (self->password_dirs));
	pg_filtered_listmodel_set_filter_func (self->password_dirs_filtered,
					       search_filter_predicate_cb,
					       g_object_ref (self),
					       g_object_unref);
	gtk_list_box_bind_model (passwords_box,
				 G_LIST_MODEL (self->password_dirs_filtered),
				 show_password_cb,
				 NULL,
				 NULL);
	gtk_list_box_set_selection_mode (passwords_box, GTK_SELECTION_NONE);

	g_signal_connect (win,
			  "key-press-event",
			  G_CALLBACK (window_key_press_event_cb),
			  self);

	g_signal_connect (self->password_search_entry,
			  "search-changed",
			  G_CALLBACK (search_changed_cb),
			  self);

	g_signal_connect (self->password_search_entry,
			  "stop-search",
			  G_CALLBACK (stop_search_cb),
			  self);

	GtkToggleButton *search_toggle = (GtkToggleButton *)
		gtk_builder_get_object (builder, "pg-searchtogglebutton");
	g_signal_connect (search_toggle,
			  "toggled",
			  G_CALLBACK (search_button_toggled_cb),
			  self->password_search_bar);

	g_signal_connect (self->password_search_bar,
			  "notify::search-mode-enabled",
			  G_CALLBACK (search_bar_mode_changed_cb),
			  search_toggle);

	gtk_application_add_window (GTK_APPLICATION (self), GTK_WINDOW (win));

	self->store = pg_store_new ();
	pg_store_get_passwords_async (self->store,
				      NULL,
				      pg_application_get_passwords_cb,
				      self);

	gtk_widget_show_all (GTK_WIDGET (win));

	g_signal_connect (passwords_box,
		  "row-activated",
		  G_CALLBACK (pg_application_activate_password_file),
		  self);
}

PgApplication *
pg_application_new (void)
{
	return g_object_new (PG_TYPE_APPLICATION,
			     "application-id", "fi.sinervo.sham1.PassGtk",
			     NULL);
}

static void
pg_application_finalize (GObject *object)
{
	PgApplication *self = (PgApplication *)object;

	g_clear_pointer (&self->current_search_term, g_free);

	G_OBJECT_CLASS (pg_application_parent_class)->finalize (object);
}

static void
pg_application_dispose (GObject *object)
{
	PgApplication *self = (PgApplication *)object;

	g_clear_object (&self->password_dirs);
	g_clear_object (&self->store);

	G_OBJECT_CLASS (pg_application_parent_class)->dispose (object);
}

static void
pg_application_class_init (PgApplicationClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GApplicationClass *g_application_class = G_APPLICATION_CLASS (klass);

	object_class->finalize = pg_application_finalize;
	object_class->dispose = pg_application_dispose;

	g_application_class->activate = pg_application_activate;
}

static void
pg_application_init (PgApplication *self)
{
}
