/* pg-filtered-listmodel.h
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>

G_BEGIN_DECLS

#define PG_TYPE_FILTERED_LISTMODEL (pg_filtered_listmodel_get_type())

G_DECLARE_FINAL_TYPE (PgFilteredListmodel,
		      pg_filtered_listmodel,
		      PG, FILTERED_LISTMODEL,
		      GObject)

typedef gboolean (*PgFilteredListmodelFilterFunc) (gpointer item,
                                                   gpointer user_data);

PgFilteredListmodel *pg_filtered_listmodel_new (GListModel *backing);

void pg_filtered_listmodel_set_filter_func (PgFilteredListmodel *list,
                                            PgFilteredListmodelFilterFunc func,
                                            gpointer user_data,
					    GDestroyNotify user_destroy);

void pg_filtered_listmodel_flush_filter (PgFilteredListmodel *self);

G_END_DECLS
