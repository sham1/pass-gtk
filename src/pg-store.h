/* pg-store.h
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
#include "pg-password-file.h"

G_BEGIN_DECLS

#define PG_TYPE_STORE (pg_store_get_type())

G_DECLARE_FINAL_TYPE (PgStore, pg_store, PG, STORE, GObject)

PgStore *pg_store_new (void);

void pg_store_get_password_async (PgStore             *store,
                                  PgPasswordFile      *password_file,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data);

gchar *pg_store_get_password_finish (PgStore       *store,
                                     GAsyncResult  *result,
                                     GError       **error);

void pg_store_get_passwords_async (PgStore             *store,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data);

GPtrArray *pg_store_get_passwords_finish (PgStore       *store,
                                          GAsyncResult  *result,
                                          GError       **error);

G_END_DECLS
