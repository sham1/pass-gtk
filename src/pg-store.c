/* pg-store.c
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pg-store.h"
#include "pg-password-file.h"

#include <gpgme.h>
#include <gpg-error.h>

#include <errno.h>

#define PG_STORE_FILE_ATTRIBUTE (G_FILE_ATTRIBUTE_STANDARD_NAME ","\
				 G_FILE_ATTRIBUTE_STANDARD_TYPE)

struct _PgStore
{
	GObject parent_instance;
	GFile *pass_directory;

	char *key_fingerprint;
	gboolean gpg_prepared;
	gpgme_ctx_t gpg_ctx;
};

G_DEFINE_TYPE (PgStore, pg_store, G_TYPE_OBJECT)

enum {
	PROP_0,
	N_PROPS
};

static GParamSpec *properties [N_PROPS];

PgStore *
pg_store_new (void)
{
	return g_object_new (PG_TYPE_STORE, NULL);
}

static void
pg_store_finalize (GObject *object)
{
	PgStore *self = (PgStore *)object;

	gpgme_release (self->gpg_ctx);
	if (self->key_fingerprint != NULL) g_free (self->key_fingerprint);

	G_OBJECT_CLASS (pg_store_parent_class)->finalize (object);
}

static void
pg_store_dispose (GObject *object)
{
	PgStore *self = (PgStore *)object;

	g_clear_object (&self->pass_directory);

	G_OBJECT_CLASS (pg_store_parent_class)->dispose (object);
}

static void
pg_store_get_property (GObject    *object,
                       guint       prop_id,
                       GValue     *value,
                       GParamSpec *pspec)
{
	//PgStore *self = PG_STORE (object);

	switch (prop_id)
	  {
	  default:
	    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	  }
}

static void
pg_store_set_property (GObject      *object,
                       guint         prop_id,
                       const GValue *value,
                       GParamSpec   *pspec)
{
	//PgStore *self = PG_STORE (object);

	switch (prop_id)
	  {
	  default:
	    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	  }
}

static void
pg_store_class_init (PgStoreClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = pg_store_finalize;
	object_class->dispose = pg_store_dispose;
	object_class->get_property = pg_store_get_property;
	object_class->set_property = pg_store_set_property;
}

static void
pg_store_init (PgStore *self)
{
	GFile *home = g_file_new_for_path (g_get_home_dir ());
	self->pass_directory = g_object_ref (
		g_file_get_child (home, ".password-store"));
	g_object_unref (home);
	self->gpg_prepared = FALSE;
}

static gint
gpg_err_to_gerror (gpg_error_t   gpg_error,
                   GError      **error)
{
	GIOErrorEnum errcode;
	switch (gpgme_err_code (gpg_error)) {
	// Special case.
	// If there was no error, we don't do anything.
	case GPG_ERR_NO_ERROR: return 0;
	// Another special case.
	// If we run out of memory, we'll just abort.
	case GPG_ERR_ENOMEM:
		g_error ("%s: out of memory",
			 gpgme_strsource (gpg_error));
	case GPG_ERR_INV_VALUE:
		errcode = G_IO_ERROR_INVALID_ARGUMENT;
		break;
	default:
		errcode = G_IO_ERROR_FAILED;
		break;
	}
	g_set_error (error, G_IO_ERROR, errcode,
		     "%s: error code (%d): %s",
		     gpgme_strsource (gpg_error),
		     gpgme_err_code (gpg_error),
		     gpgme_strerror (gpg_error));
	return 1;
}

/**
 * pg_store_get_password_async:
 * @store: input #PgStore
 * @path: a path to the password within the store
 * @cancellable: (allow-none): optional #GCancellable object, %NULL to ignore
 * @callback: (scope async): a #GAsyncReadyCallback to call when the
 *   request is satisfied
 * @user_data: (closure): the data to pass to callback function
 *
 * Fetches a password from the password store.
 *
 * When the operation is finished, @callback will be called. You can then call
 *   pg_store_get_password_finish() to get the results of the operation.
 */
void
pg_store_get_password_async (PgStore             *store,
                             PgPasswordFile      *password_file,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
	g_autoptr (GError) error = NULL;
	g_autoptr (GTask) task = NULL;

	g_assert (PG_IS_STORE (store));
	g_assert (PG_IS_PASSWORD_FILE (password_file));
	g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

	task = g_task_new (store, cancellable, callback, user_data);
	gpgme_error_t gpg_err;
	if (!store->gpg_prepared) {
		gpgme_check_version (NULL);

		gpg_err = gpgme_new (&store->gpg_ctx);
		if (gpg_err_to_gerror (gpg_err, &error) > 0) {
			g_task_return_error (task, g_steal_pointer (&error));
			return;
		}

		GFile *gpg_id_file = g_file_get_child (store->pass_directory,
					       ".gpg-id");
		if (g_file_query_exists (gpg_id_file, cancellable)) {
			GFileInputStream *stream =
				g_file_read (gpg_id_file, cancellable, &error);
			if (stream == NULL) {
				g_task_return_error (task,
						     g_steal_pointer (&error));
				g_object_unref (gpg_id_file);
				return;
			}
			gchar buf[255];
			gssize read_count = 0;
			read_count = g_input_stream_read (
				G_INPUT_STREAM (stream),
				buf,
				G_N_ELEMENTS (buf) - 1,
				cancellable,
				&error);
			if (read_count > 0) {
				for (size_t i = 0; i < read_count; ++i) {
					if (buf[i] == '\n') {
						buf[i] = '\0';
						break;
					}
				}
				store->key_fingerprint =
					g_strdup (buf);
			} else if (read_count < 0) {
				g_task_return_error (task,
						     g_steal_pointer (&error));
				g_object_unref (gpg_id_file);
				return;
			}
		}
		g_object_unref (gpg_id_file);

		store->gpg_prepared = TRUE;
	}
	g_autoptr (GFile) pass_file = NULL;
	pass_file = g_file_get_child (store->pass_directory,
				      pg_password_file_get_backing_file
				      (password_file));

	gpgme_data_t in, out;
	gchar *passwd_path = g_file_get_path (pass_file);
	gpg_err = gpgme_data_new_from_file (&in, passwd_path, 1);
	if (gpg_err_to_gerror (gpg_err, &error) > 0) {
		g_task_return_error (task, g_steal_pointer (&error));
		return;
	}

	gpg_err = gpgme_data_new (&out);
	if (gpg_err_to_gerror (gpg_err, &error) > 0) {
		g_task_return_error (task, g_steal_pointer (&error));
		return;
	}

	gpg_err = gpgme_op_decrypt (store->gpg_ctx, in, out);
	if (gpg_err_to_gerror (gpg_err, &error) > 0) {
		g_task_return_error (task, g_steal_pointer (&error));
		goto clean;
	}

	gpgme_decrypt_result_t result =
		gpgme_op_decrypt_result (store->gpg_ctx);
	if (result->unsupported_algorithm) {
		fprintf (stderr, "Unsupported decryption algorithm: %s\n",
			 result->unsupported_algorithm);
	}

	gchar *pass_buf = NULL;
	size_t pass_len = 0;

	off_t file_pos = gpgme_data_seek (out, SEEK_SET, 0);
	if (file_pos == -1) {
		int tmp_errno = errno;
		GIOErrorEnum errcode = g_io_error_from_errno (errno);
		error = g_error_new (G_IO_ERROR, errcode,
				     "Failed to read password: %s",
				     g_strerror (tmp_errno));
		g_task_return_error (task, g_steal_pointer (&error));
		goto clean;
	}

	while (TRUE) {
		gchar tmp_buf[256] = { 0 };
		ssize_t read_count = gpgme_data_read (out,
						      tmp_buf,
						      G_N_ELEMENTS
						      (tmp_buf) - 1);
		if (read_count == -1) {
			int tmp_errno = errno;
			GIOErrorEnum errcode = g_io_error_from_errno (errno);
			error = g_error_new (G_IO_ERROR, errcode,
					     "Failed to read password: %s",
					     g_strerror (tmp_errno));
			g_task_return_error (task, g_steal_pointer (&error));
			goto clean;
		}
		if (read_count == 0) {
			break;
		}
		gchar *tmp = pass_buf == NULL ? g_strdup ("") : pass_buf;
		pass_buf = g_strdup_printf ("%s%s", tmp, tmp_buf);
		if (tmp != NULL) g_free (tmp);
		pass_len += read_count;
	}
	gchar *tmp = pass_buf;
	for (size_t i = 0; i < pass_len; ++i) {
		if (pass_buf[i] == '\n') {
			pass_buf[i] = '\0';
			break;
		}
	}
	pass_buf = g_strdup (pass_buf);
	g_free (tmp);

	g_task_return_pointer (task, pass_buf, g_free);

clean:
	gpgme_data_release (in);
	gpgme_data_release (out);

}

/**
 * pg_store_get_password_finish:
 * @store: input #PgStore
 * @result: a #GAsyncResult
 * @error: a #GError
 *
 * Finishes an async password fetch operation.
 *   See pg_store_get_password_async().
 *
 * Returns: the password or %NULL if an error occured.
 *   Free the returned string with g_free().
 */
gchar *
pg_store_get_password_finish (PgStore       *store,
                              GAsyncResult  *result,
                              GError       **error)
{
	return g_task_propagate_pointer (G_TASK (result), error);
}

struct pg_store_get_passwords_internal
{
	GSList *list;
	GPtrArray *parr;
	GFile *pass_directory;
};

static void
pg_store_get_passwords_internal_destroy (gpointer data)
{
	struct pg_store_get_passwords_internal *psgpi = data;
	g_ptr_array_unref (psgpi->parr);
	g_slist_free_full (psgpi->list,
			   (GDestroyNotify) g_object_unref);
	g_clear_object (&psgpi->pass_directory);
	g_free (psgpi);
}

static void
pg_store_get_passwords_async_cb (GObject      *obj,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
	g_autoptr (GTask) task = user_data;
	GFile *dir = (GFile *) obj;

	g_assert (G_IS_FILE (dir));
	g_assert (G_IS_TASK (task));

	struct pg_store_get_passwords_internal *internal_data =
		g_task_get_task_data (task);
	if (internal_data->parr == NULL)
		internal_data->parr = g_ptr_array_new ();

	gchar *current_prefix =
		g_file_get_relative_path (internal_data->pass_directory, dir);
	g_autoptr (GError) error = NULL;
	GFileEnumerator *direnum =
		g_file_enumerate_children_finish (dir,
						  result,
						  &error);
	if (direnum == NULL) {
		g_task_return_error (task, g_steal_pointer(&error));
		goto cleanup;
	}

	while (TRUE) {
		GFileInfo *info = NULL;

		if (!g_file_enumerator_iterate (direnum,
						&info,
						NULL,
						g_task_get_cancellable (task),
						&error)) {
			g_task_return_error (task, error);
			goto cleanup2;
		}
		if (info == NULL) break;
		const gchar *name = g_file_info_get_name (info);
		GFileType type = g_file_info_get_file_type (info);
		if (g_strcmp0 (name, ".git") == 0) continue;
		if (g_strcmp0 (name, ".extensions") == 0) continue;
		if (g_strcmp0 (name, ".gitattributes") == 0) continue;
		if (g_strcmp0 (name, ".gpg-id") == 0) continue;
		if (type == G_FILE_TYPE_DIRECTORY) {
			internal_data->list =
				g_slist_append (internal_data->list,
						g_file_get_child (dir, name));
		}
		if (type == G_FILE_TYPE_REGULAR) {
			gchar *prefix = NULL;
			if (current_prefix == NULL) {
				prefix = g_strdup ("");
			} else {
				prefix =
					g_strdup_printf ("%s/",
							 current_prefix);
			}
			gchar *tmp = g_strdup_printf ("%s%s",
						      prefix,
						      name);
			PgPasswordFile *file =
				pg_password_file_new (tmp);
			g_ptr_array_add (internal_data->parr,
					 file);
			g_free (tmp);
			g_free (prefix);
		}
	}
	if (internal_data->list == NULL) {
		g_ptr_array_set_free_func (internal_data->parr, g_object_unref);
		g_task_return_pointer (task,
				       g_ptr_array_ref (internal_data->parr),
				       (GDestroyNotify) g_ptr_array_unref);
		goto cleanup2;
	}
	GFile *next = internal_data->list->data;
	internal_data->list = g_slist_remove (internal_data->list, next);

	GCancellable *c = g_task_get_cancellable (task);
	g_file_enumerate_children_async (next,
					 PG_STORE_FILE_ATTRIBUTE,
					 G_FILE_QUERY_INFO_NONE,
					 G_PRIORITY_LOW,
					 c,
					 pg_store_get_passwords_async_cb,
					 g_steal_pointer (&task));
cleanup2:
	g_object_unref (direnum);
cleanup:
	g_free (current_prefix);
}

/**
 * pg_store_get_passwords_async:
 * @store: input #PgStore
 * @cancellable: (allow-none): optional #GCancellable object, %NULL to ignore
 * @callback: (scope async): a #GAsyncReadyCallback to call when the
 *   request is satisfied.
 * @user_data: (closure): the data to pass to callback function
 *
 * Gets a list of passwords from the password store.
 *
 * When the operation is finished, @callback will be called. You can then call
 *   pg_store_get_passwords_finish() to get the results of the operation.
 */
void pg_store_get_passwords_async (PgStore             *store,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
	g_assert (PG_IS_STORE (store));
	g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

	g_autoptr (GTask) task = NULL;
	task = g_task_new (store, cancellable, callback, user_data);

	g_task_set_priority (task, G_PRIORITY_LOW);
	g_task_set_source_tag (task, pg_store_get_passwords_async);

	struct pg_store_get_passwords_internal *internal_data =
		g_malloc0 (sizeof (*internal_data));
	internal_data->pass_directory = store->pass_directory;
	g_task_set_task_data (task, internal_data,
			      pg_store_get_passwords_internal_destroy);

	g_file_enumerate_children_async (store->pass_directory,
					 PG_STORE_FILE_ATTRIBUTE,
					 G_FILE_QUERY_INFO_NONE,
					 G_PRIORITY_LOW,
					 cancellable,
					 pg_store_get_passwords_async_cb,
					 g_steal_pointer (&task));
}

/**
 * pg_store_get_passwords_finish:
 *
 * Finishes an async password listing operation.
 *   See pg_store_get_passwords_async().
 *
 * Returns: (transfer container) (element-type #Pg.PasswordFile):
 *   an array of password files or %NULL if an error occured.
 *   Free the returned array with g_ptr_array_unref().
 */
GPtrArray *
pg_store_get_passwords_finish (PgStore       *store,
                               GAsyncResult  *result,
                               GError       **error)
{
	return g_task_propagate_pointer (G_TASK (result), error);
}
