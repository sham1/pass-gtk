/* pg-password-file.c
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pg-password-file.h"

struct _PgPasswordFile
{
	GObject parent_instance;

	gchar *backing_file;
};

G_DEFINE_TYPE (PgPasswordFile, pg_password_file, G_TYPE_OBJECT)

enum {
	PROP_0,
	BACKING_FILE,
	N_PROPS
};

static GParamSpec *properties [N_PROPS];

PgPasswordFile *
pg_password_file_new (gchar *file)
{
	return g_object_new (PG_TYPE_PASSWORD_FILE,
			     "backing-file", file,
			     NULL);
}

static void
pg_password_file_finalize (GObject *object)
{
	PgPasswordFile *self = (PgPasswordFile *)object;

	g_free (self->backing_file);

	G_OBJECT_CLASS (pg_password_file_parent_class)->finalize (object);
}

static void
pg_password_file_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
	PgPasswordFile *self = PG_PASSWORD_FILE (object);

	switch (prop_id)
	{
	case BACKING_FILE:
		g_value_set_string (value, self->backing_file);
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
pg_password_file_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
	PgPasswordFile *self = PG_PASSWORD_FILE (object);

	switch (prop_id)
	{
	case BACKING_FILE:
		self->backing_file = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
pg_password_file_class_init (PgPasswordFileClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = pg_password_file_finalize;
	object_class->get_property = pg_password_file_get_property;
	object_class->set_property = pg_password_file_set_property;

	properties[BACKING_FILE] =
		g_param_spec_string ("backing-file",
				     "The backing file",
				     "The actual file that backs this password",
				     "",
				     G_PARAM_READWRITE |
				     G_PARAM_CONSTRUCT_ONLY);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

/**
 * pg_password_file_get_backing_file:
 * @password_file: This password file
 *
 * This function gives the path within the password manager's database
 * corresponding to this #PgPasswordFile
 *
 * Returns: (transfer none): the child path within the password database
 */
gchar *
pg_password_file_get_backing_file (PgPasswordFile *password_file)
{
	return password_file->backing_file;
}

static void
pg_password_file_init (PgPasswordFile *self)
{
}
