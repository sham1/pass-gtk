/* pg-filtered-listmodel.c
 *
 * Copyright 2018 Jani Juhani Sinervo <jani@sinervo.fi>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pg-filtered-listmodel.h"

struct _PgFilteredListmodel
{
	GObject parent_instance;

	GListModel *backing;

	GListStore *cache;

	PgFilteredListmodelFilterFunc filter_func;

	gpointer filter_func_user_data;
	GDestroyNotify filter_func_user_data_destroy_notify;
};

static void pg_filtered_listmodel_lm_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (PgFilteredListmodel,
			 pg_filtered_listmodel,
			 G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL,
						pg_filtered_listmodel_lm_iface_init))

enum {
	PROP_0,
	PROP_BACKING,
	N_PROPS
};

static GParamSpec *properties [N_PROPS];

PgFilteredListmodel *
pg_filtered_listmodel_new (GListModel *backing)
{
	g_return_val_if_fail (backing != NULL, NULL);

	return g_object_new (PG_TYPE_FILTERED_LISTMODEL,
			     "backing", backing,
			     NULL);
}

static void
pg_filtered_listmodel_finalize (GObject *object)
{
	//PgFilteredListmodel *self = (PgFilteredListmodel *)object;

	G_OBJECT_CLASS (pg_filtered_listmodel_parent_class)->finalize (object);
}

static void
pg_filtered_listmodel_dispose (GObject *object)
{
	PgFilteredListmodel *self = (PgFilteredListmodel *)object;

	g_clear_object (&self->backing);
	g_clear_object (&self->cache);

	if (self->filter_func_user_data &&
	    self->filter_func_user_data_destroy_notify) {
		(self->filter_func_user_data_destroy_notify)
		    (self->filter_func_user_data);
		self->filter_func_user_data = NULL;
		self->filter_func_user_data_destroy_notify = NULL;
	}

	self->filter_func = NULL;

	G_OBJECT_CLASS (pg_filtered_listmodel_parent_class)->dispose (object);
}

static void
pg_filtered_listmodel_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
	PgFilteredListmodel *self = PG_FILTERED_LISTMODEL (object);

	switch (prop_id)
	{
	case PROP_BACKING:
		g_value_set_object (value, self->backing);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}

static void
pg_filtered_listmodel_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
	PgFilteredListmodel *self = PG_FILTERED_LISTMODEL (object);

	switch (prop_id)
	{
	case PROP_BACKING:
		self->backing = g_value_dup_object (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
	}
}


static void pg_filtered_listmodel_constructed (GObject *obj);

static void
pg_filtered_listmodel_class_init (PgFilteredListmodelClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = pg_filtered_listmodel_finalize;
	object_class->get_property = pg_filtered_listmodel_get_property;
	object_class->set_property = pg_filtered_listmodel_set_property;
	object_class->dispose = pg_filtered_listmodel_dispose;
	object_class->constructed = pg_filtered_listmodel_constructed;

	properties[PROP_BACKING] = g_param_spec_object ("backing",
							"Backing GListModel",
							"Backing GListModel",
							G_TYPE_LIST_MODEL,
							G_PARAM_READWRITE |
							G_PARAM_CONSTRUCT_ONLY|
							G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties (object_class, N_PROPS, properties);
}

void
pg_filtered_listmodel_flush_filter (PgFilteredListmodel *self)
{
	guint n_old = g_list_model_get_n_items (G_LIST_MODEL (self->cache));

	g_list_store_remove_all (self->cache);

	guint len = g_list_model_get_n_items (G_LIST_MODEL (self->backing));
	for (guint i = 0; i < len; ++i) {
		gboolean should_include = TRUE;
		gpointer item = g_list_model_get_object (G_LIST_MODEL
							 (self->backing), i);
		if (self->filter_func != NULL) {
			should_include = (self->filter_func)
						(item,
						 self->filter_func_user_data);
		}

		if (should_include) {
			g_list_store_append (self->cache, item);
		}
	}

	guint n_new = g_list_model_get_n_items (G_LIST_MODEL (self->cache));

	g_list_model_items_changed (G_LIST_MODEL (self), 0, n_old, n_new);
}

static void
pg_filtered_listmodel_backing_items_changed_cb (GListModel *list,
                                                guint       position,
                                                guint       removed,
                                                guint       added,
                                                gpointer    user_data)
{
	PgFilteredListmodel *self = user_data;
	pg_filtered_listmodel_flush_filter (self);
}

static void
pg_filtered_listmodel_init (PgFilteredListmodel *self)
{
}

static GType
pg_filtered_listmodel_get_item_type (GListModel *model)
{
	PgFilteredListmodel *self = (PgFilteredListmodel *) model;

	g_assert (PG_IS_FILTERED_LISTMODEL (self));

	return g_list_model_get_item_type (G_LIST_MODEL (self->cache));
}

static guint
pg_filtered_listmodel_get_n_items (GListModel *model)
{
	PgFilteredListmodel *self = (PgFilteredListmodel *) model;

	g_assert (PG_IS_FILTERED_LISTMODEL (self));

	return g_list_model_get_n_items (G_LIST_MODEL (self->cache));
}


static gpointer
pg_filtered_listmodel_get_item (GListModel *model,
                                guint       position)
{
	PgFilteredListmodel *self = (PgFilteredListmodel *) model;

	g_assert (PG_IS_FILTERED_LISTMODEL (self));

	return g_list_model_get_item (G_LIST_MODEL (self->cache),
				      position);
}


static void
pg_filtered_listmodel_lm_iface_init (GListModelInterface *iface)
{
	iface->get_item_type = pg_filtered_listmodel_get_item_type;
	iface->get_n_items = pg_filtered_listmodel_get_n_items;
	iface->get_item = pg_filtered_listmodel_get_item;
}

void
pg_filtered_listmodel_set_filter_func (PgFilteredListmodel           *self,
                                       PgFilteredListmodelFilterFunc  func,
                                       gpointer                       user_data,
                                       GDestroyNotify                 destroy)
{
	g_return_if_fail (self != NULL);
	self->filter_func = func;
	self->filter_func_user_data = user_data;
	self->filter_func_user_data_destroy_notify = destroy;
}

static void
pg_filtered_listmodel_constructed (GObject *obj)
{
	PgFilteredListmodel *self = (PgFilteredListmodel *) obj;

	g_assert (PG_IS_FILTERED_LISTMODEL (self));

	GType list_item_type = g_list_model_get_item_type (self->backing);
	self->cache = g_list_store_new (list_item_type);

	g_signal_connect (self->backing,
			  "items-changed",
			  G_CALLBACK
			    (pg_filtered_listmodel_backing_items_changed_cb),
			  self);

	pg_filtered_listmodel_flush_filter (self);
}
